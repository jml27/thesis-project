# This code is a modification from the code found at the following source:
# https://github.com/cran/depmixS4/blob/master/R/viterbi.R

viterbiAlg <- function (object,densMatrix, na.allow = TRUE) 
{
  nt <- sum(object@ntimes)+nrow(densMatrix) 
  lt <- 1
  et <- cumsum(object@ntimes)+nrow(densMatrix) 
  bt <- 1
  ns <- object@nstates
  delta <- psi <- matrix(nrow = nt, ncol = ns)
  state <- vector(length = nt)
  prior <- object@init
  if (max(nrow(densMatrix) > 1))
    A <- object@trDens
  B <- object@dens
  if (na.allow) 
    B <- replace(B, is.na(B), 1)
  B <- apply(B, c(1, 3), prod)
  B <- rbind(B,densMatrix)
  for (case in 1:lt) {
    delta[bt[case], ] <- prior[case, ] * B[bt[case], ]
    delta[bt[case], ] <- delta[bt[case], ]/(sum(delta[bt[case], 
                                                      ]))
    psi[bt[case], ] <- 0
    if (nrow(densMatrix)[case] > 1) {
      for (tt in ((bt[case] + 1):et[case])) {
        for (j in 1:ns) {
          if (!object@homogeneous) {
            delta[tt, j] <- max(delta[tt - 1, ] * (A[tt, 
                                                     j, ])) * B[tt, j]
            k <- which.max(delta[tt - 1, ] * A[tt, j, 
                                               ])
          }
          else {
            delta[tt, j] <- max(delta[tt - 1, ] * (A[1, 
                                                     j, ])) * B[tt, j]
            k <- which.max(delta[tt - 1, ] * A[1, j, 
                                               ])
          }
          if (length(k) == 0) 
            k <- 0
          psi[tt, j] <- k
        }
        delta[tt, ] <- delta[tt, ]/(sum(delta[tt, ]))
      }
    }
    state[et[case]] <- which.max(delta[et[case], ])
    if (nrow(densMatrix)[case] > 1) {
      for (i in (et[case] - 1):bt[case]) {
        state[i] <- psi[i + 1, state[i + 1]]
      }
    }
  }
  colnames(delta) <- paste("S", 1:ns, sep = "")
  delta <- data.frame(state, delta)
  return(tail(delta,n=nrow(densMatrix)))
}
