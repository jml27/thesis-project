#-----------------------------------------------
# COMPUTE THE NEXT ALPHA, BETA and XI
#-----------------------------------------------

# Forward backward and gamma probabilities
alpha <- forwardbackward(fit.mod)$alpha
beta <- forwardbackward(fit.mod)$beta
gamma <- fb$gamma

# Get the forward probability at the current time t
currentAlpha <- alpha[length(y),]

# Find the next time t+1 forward probabilities for all observation symbols
nState <- fit.mod@nstates
nextAlpha <- matrix(0,nrow = fit.mod@nstates,ncol = uniqVal)
nextAlphaScaled <- matrix(0,nrow = nState,ncol = uniqVal)
for(j in 1:uniqVal){
  for(i in 1:nState){
    nextAlpha[i,j] <- sum(transitionMatrix[i,]*currentAlpha)*emissionMatrix[i,j]
  }
}

for(j in 1:uniqVal){
  for(i in 1:nState){
    nextAlphaScaled[i,j] <- nextAlpha[i,j]*(1/sum(nextAlpha[,j]))
  }
}

# Find the next time t+1 backward probabilities for all observation symbols
nextBeta <- colSums(nextAlpha)
nextBeta <- 1/nextBeta
nextBetaMatrix <- matrix(0,nrow = nState,ncol = uniqVal)
for(j in 1:uniqVal){
  for(i in 1:nState){
    nextBetaMatrix[i,j] <- nextBeta[j]
  }
}

# Update the previous backward probabilities
PreviousBeta <- matrix(0,nrow = 6,ncol = 10)
for(j in 1:10){
  for(i in 1:6){
    PreviousBeta[i,j] <- nextBeta[2,]*emissionMatrix[i,j]*transitionMatrix[,5,i]*fb$sca[1]
  }
}

# Find the next xi probability (i.e. probability of being in state i at time T and state j at time T+1)
for(i in 1:uniqVal){
  currentXi <- rep(currentAlpha,each=nState)*(nextBetaMatrix[,i]*emissionMatrix[,i]*transitionMatrix)
  print(currentXi)
}

# Find the probability of the next state value in the state sequence
x <- probs$state
lastState <- tail(x,n=1)
seqProb <- vector()
for(s in 1:fm2@nstates){
  x <- c(x,i)
  p <- matrix(nrow = 6, ncol = 6, 0)
  for (t in 1:(length(x)-1)) p[x[t], x[t + 1]] <- p[x[t], x[t + 1]] + 1
  for (i in 1:6) p[i, ] <- p[i, ] / sum(p[i, ])
  seqProb <- c(seqProb,p[lastState,s])
  x <- probs$state
}

# Get the prediction
#seqProb%*%nextAlphaScaled
